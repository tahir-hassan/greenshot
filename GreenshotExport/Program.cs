﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Greenshot.Plugin;
using Greenshot.Drawing;
using GreenshotPlugin.Core;

namespace GreenshotExport
{
    class Program
    {
        static void WriteLine(string message, ConsoleColor c = ConsoleColor.White)
        {
            var before = Console.ForegroundColor;
            Console.ForegroundColor = c;
            Console.WriteLine(message);
            Console.ForegroundColor = before;
        }

        static void Main(string[] args)
        {
            // will be tidied up later
            if (args.Count() == 0 || args.Count() > 2)
            {
                WriteLine("Syntax: ", ConsoleColor.Red);
                WriteLine("GreenshotExport.exe <greenshot-file> [<output-file-or-dir>]", ConsoleColor.Red);
            }
            else
            {
                var filePath = args[0];

                if (string.IsNullOrWhiteSpace(filePath))
                {
                    WriteLine("file name is empty string", ConsoleColor.Red);
                }
                else 
                {
                    var inputFilePath = Path.GetFullPath(filePath);
                    if (!File.Exists(inputFilePath)) 
                    {
                        WriteLine("file does not exist", ConsoleColor.Red);
                    }
                    else 
                    {
                        if (new FileInfo(inputFilePath).Extension.ToLower() != ".greenshot") 
                        {
                            WriteLine("file must be a greenshot file", ConsoleColor.Red);
                        }
                        else 
                        {
                            string outputFilePath = null;

                            if (args.Count() == 1)
                            {
                                var inInfo = new FileInfo(inputFilePath);
                                outputFilePath = Path.Combine(inInfo.Directory.FullName, inInfo.Name.Replace(inInfo.Extension, ".png"));
                            }
                            else if (args.Count() == 2) 
                            {
                                var outputFileOrDir = Path.GetFullPath(args[1]);

                                if (File.Exists(outputFileOrDir))
                                {
                                    outputFilePath = outputFileOrDir;
                                }
                                else if (Directory.Exists(outputFileOrDir)) 
                                {
                                    outputFilePath = Path.Combine(outputFileOrDir, new FileInfo(inputFilePath).Name);
                                }
                                else
                                {
                                    var directory = new FileInfo(outputFileOrDir).Directory;
                                    if (!directory.Exists) 
                                    {
                                        WriteLine(string.Format("directory {0} does not exist", directory.FullName), ConsoleColor.Red);
                                        return;
                                    }
                                    else 
                                    {
                                        outputFilePath = outputFileOrDir;
                                    }
                                }
                            }

                            ISurface surface = ImageOutput.LoadGreenshotSurface(inputFilePath, new Surface());

                            var outputSettings = new SurfaceOutputSettings
                            {
                                Format = ImageOutput.FormatForFilename(outputFilePath)
                            };

                            GreenshotPlugin.Core.ImageOutput.Save(surface, outputFilePath, true, outputSettings, copyPathToClipboard: false);
                            
                        }
                    }
                }
            }
        }
    }
}
